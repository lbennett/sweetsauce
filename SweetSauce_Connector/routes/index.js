var express = require('express');
var request = require('request');
var fs = require('fs');
var router = express.Router();

var remoteServer = "http://192.168.1.14:4444/";
var rootSauce = "http://sweetsauce.com:3000";
var authorizationServerUrl = rootSauce + "/api/token/validate";
var postSessionDataServerUrl = rootSauce + "/api/session/:session_id/save";

router.get("/*", function(req, res, next) {
	callRemoteSeleniumServer("GET",req.body,req.params,function(responseBody){
		res.json(responseBody);
	});
});

router.delete("/*", function(req, res, next){
	callRemoteSeleniumServer("DELETE",req.body,req.params,function(responseBody){
		res.json(responseBody);
	});
});

router.post("/*", function(req, res, next) {
	if(req.headers.authorization) {
		var emailAccessTokenDecoded = new Buffer(req.headers.authorization.split(" ")[1], 'base64').toString().split(":");
		var emailFromAuth = emailAccessTokenDecoded[0];
		var accessTokenFromAuth = emailAccessTokenDecoded[1];
		request({
			url: authorizationServerUrl,
			method: "POST",
			json: true,
			body: {email:emailFromAuth, access_token: accessTokenFromAuth}
			}, function (error, remoteRes, body){
				if(!error){
					if(remoteRes.body.hasOwnProperty("status")){
						if(remoteRes.body.status == "success") {
							callRemoteSeleniumServer("POST",req.body,req.params,function(responseBody){
								res.json(responseBody);
							});
						} else {
							res.json({"status":"error","message":"remote machine unreachable"});			
						}
					}
				} else {
					res.json({status:"BAD"});
				}
			}
		);
	} else {
		callRemoteSeleniumServer("POST",req.body,req.params,function(responseBody){
			res.json(responseBody);
		});
	}
});

var checkForVMInfoInRequest = function(passThroughCommands, passThroughUnclaimedProperties, type, body, params, callback){
	var currentSavedDirectionIN = !body.hasOwnProperty('sessionId');
	var containsClick = false;
	//command stuff
	if(currentSavedDirectionIN) {
		var url = params["0"],
			delimeter = "/",
			start = 4,
			// make an array of splitting the string at the 4th item.
			tokens = url.split(delimeter).slice(start),
			result = tokens.join(delimeter);
		if(!result) {
			result = "session";
		}
		if(type == "GET") {
			
			passThroughCommands.request = "GET " + result;
		} else {
			passThroughCommands.request = "POST " + result;
			var currentCommandDetails = [passThroughCommands.request];
			var s = "";
			if(passThroughCommands.request.toLowerCase().indexOf("click") != -1){
				containsClick = true;
			}
			for(s in body){
				currentCommandDetails.push(s + ":" + JSON.stringify(body[s]));
			}
			passThroughCommands.request = currentCommandDetails;
			passThroughCommands.screenshot = {capable:containsClick};
		}
	} else {
		passThroughCommands.response = JSON.stringify(body.value);
	}
	//end command stuff
	var toSend = {};
	if(body.hasOwnProperty('sessionId')){
		var testValuesNeeded = [
			"platform",
     		"browserName",
     		"version"
		];
		var desiredCapabilitiesNeeded = [
			"name"
		]
		toSend.sessionId = body.sessionId;
		if(body.hasOwnProperty('value')){
			var s = '';
			toSend.test = {};
			for(s in body.value){
				if(testValuesNeeded.indexOf(s) != -1) {
					toSend.test[s] = body.value[s];
				}
			};
			s = '';
			for(s in body) {
				if(testValuesNeeded.indexOf(s) != -1) {
					toSend.test[s] = body.value[s];
				}
			}
			s = '';
			var didLoopUnClaimedProperties = false;
			var loopThroughThis = passThroughUnclaimedProperties;
			if(passThroughUnclaimedProperties.hasOwnProperty('desiredCapabilities')) {
				loopThroughThis = passThroughUnclaimedProperties.desiredCapabilities;
			}
			for(s in loopThroughThis){
				didLoopUnClaimedProperties = true;
				if(desiredCapabilitiesNeeded.indexOf(s) != -1) {
					if(!toSend.hasOwnProperty(s)){
						toSend.test[s] = loopThroughThis[s];	
					}
					
				}
			}
			if(didLoopUnClaimedProperties){
				passThroughUnclaimedProperties = {};	
			}
		}
		if(passThroughCommands.hasOwnProperty('request') && passThroughCommands.hasOwnProperty('response')){
			toSend.command = passThroughCommands;
		}
		if(toSend.command.request[0].toLowerCase().indexOf('click') != -1){
			takeScreenshot(toSend, function(toSendWithScreenshot){
				postSessionData(passThroughCommands, passThroughUnclaimedProperties, body, toSendWithScreenshot, callback);	
			});	
		} else {
			postSessionData(passThroughCommands, passThroughUnclaimedProperties, body, toSend, callback);
		}
		
		

	} else {
		passThroughUnclaimedProperties = body;
		if(passThroughCommands.hasOwnProperty('request') && passThroughCommands.hasOwnProperty('response')){
			toSend.command = passThroughCommands;
			postSessionData(passThroughCommands, passThroughUnclaimedProperties, body, toSend, callback);
		} else {
			callback(passThroughCommands, passThroughUnclaimedProperties, body);
		}
		
	}
}


var postSessionData = function(passThroughCommands, passThroughUnclaimedProperties, body, data, callback) {
	var sessionId;
	if(data.hasOwnProperty('sessionId')){
		sessionId = data.sessionId;
		delete data.sessionId;
	}
	request({
		url: postSessionDataServerUrl.replace(":session_id",sessionId),
		method: "POST",
		json: true,
		body: data
	}, function(err, res, resBody){
		callback(passThroughCommands, passThroughUnclaimedProperties, body);
	});
}

var takeScreenshot = function(toSend, callback) {
	// console.log(toSend);
	var screenshotURL = remoteServer + "wd/hub/session/:sessionId/screenshot".replace(":sessionId",toSend.sessionId);
	request({
		url: screenshotURL, 
		method: "GET",
		json: true, 
		body: {}, 
	}, function(err, res, ssBody){
		if(res.body.hasOwnProperty('state') && res.body.state == 'success'){
			if(res.body.hasOwnProperty('value')){
				var s = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
				var randomString = Array(10).join().split(',').map(function() { return s.charAt(Math.floor(Math.random() * s.length)); }).join('');
				var filePath = '/Users/jschatz1/projects/angular/ss_connect/public/images/ss/ss_' + randomString + '.png';
				fs.writeFile(filePath, res.body.value, 'base64', function(err) {
					if(err) { 
						toSend.screenshot = null;
						return callback(toSend); 
					} else {
						toSend.screenshot = 'ss_' + randomString + '.png';
						callback(toSend);	
					}
					
				});
			} else {
				toSend.screenshot = null;
				callback(toSend);
			}
		} else {
			toSend.screenshot = null
			callback();
		}
		
	});
}

var saveScreenshot = function(base64, callback) {
	function decodeBase64Image(dataString) {
		var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
		response = {};

		if (matches.length !== 3) {
			return new Error('Invalid input string');
		}

		response.type = matches[1];
		response.data = new Buffer(matches[2], 'base64');

		return response;
	}
	var imageBuffer = decodeBase64Image(base64);
	var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	var randomString = Array(10).join().split(',').map(function() { return s.charAt(Math.floor(Math.random() * s.length)); }).join('');
	fs.writeFile('/Users/jschatz1/projects/angular/ss_connect/public/images/ss/ss_' + randomString + '.png', imageBuffer.data, function(err) {
		if(err) {
			// console.log("Error saving file.");
		} else {
			// console.log("File ss_" + randomString + ".png saved successfully.");
		}
		callback();
	});
}

var callRemoteSeleniumServer = function(type, body, params, callback) {
	var remoteURL = populateRemoteServerUrl(remoteServer, params);
	var passThroughCommands = {};
	var passThroughUnclaimedProperties = {};
	checkForVMInfoInRequest(passThroughCommands, 
		passThroughUnclaimedProperties, 
		type, 
		body, 
		params, 
		function(requestPassedThroughCommands, requestPassThroughUnclaimedProperties, prePassedBody){
		request({
			url: remoteURL,
			method: type,
			json: true,
			body: prePassedBody
			}, function (error, remoteRes, body){
				if(!error){
					checkForVMInfoInRequest(requestPassedThroughCommands, 
						requestPassThroughUnclaimedProperties, 
						type, 
						remoteRes.body, 
						params, 
						function(responsePassedThroughCommands,responsePassedThroughUnclaimedProperties, passedBody){
						callback(passedBody);
					});
				} else {
					callback({"status":"error","message":"remote machine unreachable"});
				}
			}
		);
	});
}

var populateRemoteServerUrl = function(remoteServer, obj) {
	var keys = Object.keys(obj);
	var finalUrl = remoteServer;
	for (var i = 0; i < keys.length; i++) {
		var val = obj[keys[i]];
		finalUrl += val;
	}
	return finalUrl
};

module.exports = router;
