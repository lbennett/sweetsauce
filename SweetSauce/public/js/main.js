$(document).ready(function() {

  $(document).ajaxSend(function(elm, xhr, s){
     if (s.type == "POST") {
        xhr.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
     }
  });

  $("a.disabled").on("click",function(e){
    e.preventDefault
  });

  // Place JavaScript code here...
  $("#show-access").on("click", function(e){
  	e.preventDefault();
    var $that = $(this);
  	$that.addClass("disabled");
    $.post("/account/access_token",function(result){
      if(result.status == "OK") {
        $("#access-token").val(result.access_token);
        $that.hide();
      } else {
        $that.text("Error error creating access token. Try again later.");
      }
    });
  });

  $("a.screenshot-icon").on("click", function(e){
    e.preventDefault();
    $("#commands li").eq($(this).closest("li").index() + 1).toggle('fast');
  }) 
});