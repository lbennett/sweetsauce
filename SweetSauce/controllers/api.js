var secrets = require('../config/secrets');
var querystring = require('querystring');
var validator = require('validator');
var async = require('async');
var request = require('request');
var Github = require('github-api');
var User = require('../models/User');
var Test = require('../models/Test');
var Command = require('../models/Command');
var _ = require('lodash');

/**
 * GET /api
 * List of API examples.
 */
  exports.getApi = function(req, res) {
    res.render('api/index', {
      title: 'API Examples'
    });
  };

/**
 * POST /validate the access token
 * Api call
 */
  exports.postValidateAccessToken = function(req, res) {
  if( req.body.hasOwnProperty("email") && req.body.hasOwnProperty("access_token") ) {
    User.findOne({ 'email' : req.body.email },'email access_token', function(err, user) {
      if(!user) {
        res.json({ status:"error", message: "invalid user" });
        return;
      }
      if(user.access_token == req.body.access_token) {
        res.json({ status:"success", message: "valid access token and email address" })
        return;
      } else {
        res.json({ status:"error", message: "invalid access token and/or email address" });
        return;
      }
    });
  } else {
    res.json({ status:"error", message: "invalid access token and/or email address" });
    return;
  }
 };

/**
 * POST /session/:session_id/save save or update a session
 * API call
 */
 exports.postSessionSave = function(req, res, next) {
  Test.findOne({sessionId:req.params.session_id},function(err, test){

    if(!test) {
      test = new Test();
    }
    var hasCommand = false;
    if(req.body.hasOwnProperty('command') 
      && req.body.command.hasOwnProperty('request') 
      && req.body.command.hasOwnProperty('response')){
      var capable = false;
      var command = new Command();
      if(req.body.command.hasOwnProperty('screenshot')){
        if(req.body.command.screenshot.capable){
          command.hasScreenShot = true;
          command.screenshot = req.body.screenshot;  
        } else {
          command.hasScreenShot = false;
        }
      } else {
        command.hasScreenShot = false;
      }
      
      command.request = req.body.command.request;
      command.response = req.body.command.response;
      command.test = test;

      hasCommand = true;
    }
    
    var hadTestValues = false;
    var acceptableTestValues = ['name','platform','browserName','version'];
    test.sessionId = req.params.session_id
    if(req.body.hasOwnProperty('test')){
      var s = ''
      for(s in req.body.test) {
        if(acceptableTestValues.indexOf(s) != -1) {
          test[s] = req.body.test[s].toLowerCase();
          hadTestValues = true
        }
      }
    }
    if(hasCommand){
      test.commands.push(command);
      command.save(function(err){
        test.save(function(err){
          if(err){
            res.json({ 'status':'error' });
          } else {
            res.json({ 'status':'success' });
          }
        });
      });  
    } else if(hadTestValues){
      test.save(function(err){
        if(err){
          res.json({ 'status':'error' });
        } else {
          res.json({ 'status':'success' });
        }
      });
    }
  });  
 }

/**
 * PUT /:email/jobs/:sessionid end the session with the id and email 
 * API call
 */
  exports.putSessionDelete = function(req, res, next) {
    // res.json({'status':'OK'});
    var passed = false;
    if(req.body.hasOwnProperty('passed')){
      passed = req.body.passed;
    }
    var foundTest;
    Test.findOne({sessionId:req.params.session_id},function(err, test){
      if(test) {
        foundTest = test;
        test.passed = passed;
        test.createdAt = Date.now();
        test.save(function(err, newTest){

        })
        return;
      }
    });

    res.json({'status':'success'});
    User.findOne({ 'email' : req.params.email },'email access_token tests', function(err, user) {
      if(!err){
        foundTest.user = user;
        foundTest.save(function(err){
          if(!err){
            user.tests.push(test);
            user.save(function(err) {
              if(!err) {
                res.json({'status':'success'});
              } else {
                res.json({'status':'error'});
              }
            });      
          }
        });
        
      } else {
        res.json({'status':'error'});
      }
    });
  };

/**
 * GET /api/github
 * GitHub API Example.
 */
  exports.getGithub = function(req, res, next) {
    var token = _.find(req.user.tokens, { kind: 'github' });
    var github = new Github({ token: token.accessToken });
    var repo = github.getRepo('sahat', 'requirejs-library');
    repo.show(function(err, repo) {
      if (err) return next(err);
      res.render('api/github', {
        title: 'GitHub API',
        repo: repo
      });
    });
  };

