var mongoose = require('mongoose');

var commandSchema = new mongoose.Schema({
	request:Array,
	response:String,
	created_at:Date,
	duration:Number,
	screenshot:String,
	hasScreenShot:Boolean,
	total_duration:Number,
	test:{ type : mongoose.Schema.ObjectId, ref : 'Test' }
});

module.exports = mongoose.model('Command', commandSchema);